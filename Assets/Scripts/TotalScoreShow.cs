﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TotalScoreShow : MonoBehaviour {
    private static TotalScoreShow instance_;
    public static TotalScoreShow Instance
    {
        get
        {
            instance_ = GameObject.FindObjectOfType<TotalScoreShow>();
            return instance_;
        }
    }

    Text text;
    void Start()
    {
        text = GetComponent<Text>();
    }

    public void SetScore(int score)
    {
        text.text = score.ToString();
    }
}
