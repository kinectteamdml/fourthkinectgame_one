﻿using UnityEngine;
using System.Collections;
using System;
[Serializable]
public class GameSessionController : MonoBehaviour {
    private static GameSessionController instance_;
    public static GameSessionController Instance{
        get{
            if (instance_ == null) 
                instance_= GameObject.FindObjectOfType<GameSessionController>();
            return instance_;
        }
    }
    public GameSession gameSession = new GameSession();
    public void Start()
    {
        Debug.Log(DateTime.Now.ToString());
        
        int id =0;
        if (Environment.GetCommandLineArgs().Length > 1) { 
            String s = Environment.GetCommandLineArgs()[1].Remove(0,1);
            int.TryParse(s, out id);
        }
        gameSession.patientId = id;
        gameSession.beginningTime = DateTime.Now.ToString();
        
    }

    public void OnApplicationQuit()
    {
        gameSession.endingTime = DateTime.Now.ToString();
        DateTime d = DateTime.Now;
        String s = d.Date.Month +"-"+ d.Date.Day+"-"+d.Date.Year+"___"+ d.Hour+"-"+d.Minute;
        gameSession.Save(s);
    }

   
  

}
