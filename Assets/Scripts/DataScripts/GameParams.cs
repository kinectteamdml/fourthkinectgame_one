﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.IO;
[XmlRoot("Params")]
public class GameParams {
    [XmlAttribute("Countdown")]
    public float gameTime = 600f;
    [XmlAttribute("Lives")]
    public int lives = 5;
    [XmlAttribute("Gap")]
    public float gap = 0.7f;


    public GameParams()
    { 
    }

    public void Save()
    {
        String path = Application.dataPath + "/../params.xml";
        Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public GameParams Load()
    {
        String path = Application.dataPath + "/../params.xml";
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        try
        {
            Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as GameParams;
            }
        }
        catch (Exception e)
        {
            GameParams gp = new GameParams();
            gp.Save();
            return gp;
        }
    }

}
